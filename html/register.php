<?php
    /***********************************************************************
     * register.php
     *
     * Implements a registration form for WidenerBoy registration with email 
     * confirmation.  Redirects user to apology.php upon error.
     **********************************************************************/
    // configuration 
    require("../includes/config.php");
    
    // require PHPMailer to prepare to send email confirmation
    require("PHPMailer/class.phpmailer.php");
    
    // if form was submitted 
    if ($_SERVER["REQUEST_METHOD"] == "POST") 
    {
        // validate submission
        if (empty($_POST["email"]))
        {
            apologize("You must provide your email address.");
        }
        else if (empty($_POST["password"]))
        {
            apologize("You must provide a password.");
        }
        
        else if ($_POST["password"]!== $_POST["confirmation"])
            apologize("The passwords you entered were not consistent.");
        
        
        // store user information      
        else 
        {
            // check email unique
            if (query("INSERT INTO users (email, hash, route) VALUES(?, ?, 0)", $_POST["email"], crypt($_POST["password"])) === false)
            {
                apologize("The email already exists.");
            }
    
            //else registration was successful, login, and proceed to route search
            else
            {
                $rows = query("SELECT LAST_INSERT_ID() AS id");
                $row = $rows[0];
                $id = $rows[0]["id"];
               
                // remember that user's now logged in by storing user's ID in session
                $_SESSION["id"] = $rows["id"];

     
                // instantiate mailer
                $mail = new PHPMailer();
                
                // use SMTP
                $mail->IsSMTP();
                $mail->Host =   "smtp.fas.harvard.edu";
                  
                // set From:
                $mail->SetFrom("jharvard@cs50.net");
                  
                // set To:
                $mail->AddAddress($_POST["email"]);

                // set Subject:
                $mail->Subject = "WidenerBoy Registration Confirmation";
                     
                // set body
                $mail->Body = "Thank you for registering at WidenerBoy! Now you can save your widener root and return it any time.\n\n" .
                    "Account: " . $_POST["email"] . "\n";

                // send mail
                if ($mail->Send() === false)
                    apologize("The confirmation email is not sent");
                
                // redirect to route search
                redirect("/");
                      
            }
        }
    }
    
    // else render register form again
    else 
    {    
        render("register_form.php", ["title" => "Register"]);
    }

?>
