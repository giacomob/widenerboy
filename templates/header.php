<!DOCTYPE html>

<html>

    <head>
        
        <link href="css/bootstrap.css" rel="stylesheet"/>
        <link href="css/bootstrap-responsive.css" rel="stylesheet"/>
        <link href="css/styles.css" rel="stylesheet"/>

        <?php if (isset($title)): ?>
            <title>WidenerBoy: <?= htmlspecialchars($title) ?></title>
        <?php else: ?>
            <title>WidenerBoy</title>
        <?php endif ?>

        <script src="js/jquery-1.8.2.js"></script>
        <script src="js/bootstrap.js"></script>
        <script src="js/scripts.js"></script>

    </head>

    <body>

        <br/>
        <br/>
        <br/>

        <div class="container-fluid">

            <div id="top">
                <a href="/"><img alt="WidenerBoy" src="img/logo.png"/></a>
            </div>

            <div id="middle">
            <br/>
            <br/>
            <br/>

   
            

