    <ul class="nav nav-pills">
        <li><a href="quote.php">Get Quote</a></li>
        <li><a href="buy.php">Buy</a></li>
        <li><a href="sell.php">Sell</a></li>
        <li><a href="add_cash.php">Add cash</a></li>
        <li><a href="history.php">History</a></li>
        <li><a href="/">Home page</a></li>
        <li><a href="logout.php">Log Out</a></li>
    </ul> 

<table class="table table-striped">
    <thead>
        <tr>
            <th>Transaction</th>            
            <th>Date/Time</th>
            <th>Symbol</th>
            <th>Name</th>
            <th>Shares</th>
            <th>Price at time of sale</th>
            <th>TOTAL</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($rows as $row): ?>
            <tr>
                <td><?= $row["transaction"] ?></td>
                <td><?= $row["date"] ?></td>                
                <td><?= $row["symbol"] ?></td>
                <td><?= $row["name"] ?></td>
                <td><?= $row["shares"] ?></td>                                
                <td>$<?= number_format($row["price"], 4) ?></td>
                <td>$<?= number_format($row["total"], 4) ?></td>                                
            </tr>                        
        <? endforeach ?>
    </tbody>
</table>

